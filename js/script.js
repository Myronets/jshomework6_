// Задание
// Реализовать функцию фильтра массива по указанному типу данных.
//
//     Технические требования:
//
//     Написать функцию filterBy(), которая будет принимать в себя 2 аргумента.
//     Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
//     Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы
//     в аргумент, за исключением тех, тип которых был передан вторым аргументом. То есть,
//     если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 'string',
//     то функция вернет массив [23, null].


/************************ через for **********************/
//
// filterBy = (array, exclusionType) => {
//     let resultArray = [];
//     for (let i=0; i < array.length; i++) {
//         if (typeof(array[i]) !== exclusionType) {
//             resultArray.push(array[i]);
//         }
//     }
//     return resultArray;
// };
//
//
// let arr = ['hello', 'world', 23, '23', null],
//     x = 'string';
//
// console.log(filterBy(arr, x));

/************************ через forEach **********************/

// filterBy = (array, exclusionType) => {
//     let resultArray = [];
//
//     array.forEach(function (item) {
//         if (typeof(item) !== exclusionType) {
//             resultArray.push(item);
//         }
//     });
//
//     return resultArray;
// };
//
//
// let arr = ['hello', 'world', 23, '23', null],
//     x = 'string';
//
// console.log(filterBy(arr, x));

/************************ через filter **********************/

filterBy = (array, exclusionType) => {
    return array.filter(function (item) {
        return typeof(item) !== exclusionType;
        });
};


let arr = ['hello', 'world', 23, '23', null],
    x = 'string';

console.log(filterBy(arr, x));